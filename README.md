# ScroogeBot
[Invite him to your server!](https://discordapp.com/oauth2/authorize?client_id=498451115038343170&scope=bot&permissions=224256)  
Bot that scrapes /r/GameDeals and sends you information about available discounts and free games. Type **$scrhelp** to get list of commands.

## Code
Bot is deployed on Heroku, should be online 24/7.
Most of it is in one file because I had troubles with splitting it into separate cogs.  
To run it locally:
 1.  Run `docker-compose up`
 2. Create `config.py` with  
`token=<YOUR_DISCORD_SEVRET_TOKEN>`  
`dbase=<LINK_TO_YOUR_DBASE_HERE>`
3. Run `main.py`
