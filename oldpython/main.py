from config import token
from config import dburl

from discord.ext import commands
import discord

import logging
import asyncio
import asyncpg
import ssl

import dealsdata
import currencyswitch

logging.basicConfig(level=logging.INFO)

# logger = logging.getLogger('discord')
# logger.setLevel(logging.INFO)
# handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
# handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
# logger.addHandler(handler)


pool = None
"""Used for DBase connections"""

serversid={0}
"""Workaround. I store server ids here. Iterating through bot.guilds can fail when in the middle of loop someone joins or leaves."""

prefix='$'
bot = commands.Bot(command_prefix=prefix)
bot.remove_command('help')

"""COMMANDS"""

@bot.command()
async def scrhelp(ctx):
    """Sends information about avalible actions"""

    embed = discord.Embed(
        title="ℹ️ Help ℹ️",
        description="You\'re here! Here you will get all info about avalible commands.",
        colour=discord.Colour.purple()
        )
    embed.add_field(name=f'💰 {prefix}getdealshere',
                    value='This command tells the bot where he should post deals. Keep in mind that commands will work ONLY in the channel where this command was used.')
    embed.add_field(name=f'"💱 {prefix}currency XXX',
                    value='Prices are shown in either $, £ or €. With this command you can add **ONE** additional currency. \nAvailable currencies:\nUSD, JPY, BGN, CZK, DKK, GBP, HUF, PLN, RON, SEK, CHF, ISK, NOK, HRK, RUB, TRY, AUD, BRL, CAD, CNY, HKD, IDR, ILS, INR, KRW, MXN, MYR, NZD, PHP, SGD, THB, ZAR.\n Type OFF to switch off currency changer')
    embed.add_field(name=f'🕹️ {prefix}consoles ON/OFF',
                    value='With ON you subscribe to console offers. With OFF you\'re in PC only mode.')
    embed.add_field(name=f'🆓 {prefix}onlyfree ON/OFF',
                    value='With this option you can decide if you want to recieve informations **only** about FREE games. Use ON to get **only** free deals, use OFF to get **all** deals available,')
    embed.add_field(name=f':chart_with_upwards_trend: {prefix}percentoff XX', value='Searches for deals that have at least <your_number>% off. Use OFF to get all available deals.')
    embed.add_field(name=f'✌️ {prefix}scrinvite', value='Sends you link to invite this bot to a new server :3')
    embed.add_field(name=f'⚙️ {prefix}currentconfig', value='Shows current settings.', inline=True)
    embed.add_field(name=f'🛑 {prefix}stop', value='Stops the bot from searching for deals.',inline=False)
    embed.add_field(name=f':question: Got more questions? Found a bug?', value=':point_right: Join [the support server](https://discord.gg/E2dZVAT) 👈',inline=False)
    embed.set_footer(text='Created by zaqs114 #5275', icon_url='https://cdn.discordapp.com/embed/avatars/0.png')
    await ctx.send(embed=embed)

@bot.command()
async def scrinvite(ctx):
    """Generates invite link."""

    if ctx.message.guild is None and ctx.message.author!= bot.user.id:
        await ctx.send(discord.utils.oauth_url(client_id='498451115038343170', permissions=discord.Permissions(224256)))
    else:
        workingchannel = await get_working_channel(ctx)
        if not ctx.message.author.permissions_in(ctx.message.channel).manage_guild:
            await ctx.send("❌ | You dont have permissions to use that command.")
        elif ctx.message.channel.id == workingchannel.id:
            await ctx.send(discord.utils.oauth_url(client_id='498451115038343170', permissions=discord.Permissions(224256)))

@bot.command()
@commands.has_permissions(manage_guild=True)
async def getdealshere(ctx):
    """Specifies channel where bot should work"""

    async with pool.acquire() as conn:
        await conn.execute('UPDATE botdata SET inloop=True, workingchannel=$2 WHERE server_id=$1', str(ctx.message.guild.id), str(ctx.message.channel.id))
    await ctx.send(":white_check_mark: | Deals will be delivered to " + str(ctx.message.channel))

@bot.command()
@commands.has_permissions(manage_guild=True)
async def stop(ctx):
    """Stops bot from working"""

    workingchannel = await get_working_channel(ctx)
    async with pool.acquire() as conn:
        if ctx.message.channel.id == workingchannel.id: #with that im checking if the command was send in channel where bot should work
            await conn.execute('UPDATE botdata SET inloop=$2 WHERE server_id=$1',
                               str(ctx.message.guild.id), False)
            await ctx.send("❌ | Bot stopped.")

@bot.command()
@commands.has_permissions(manage_guild=True)
async def onlyfree(ctx, arg):
    """Checks if there are 100% free deals. False by default"""

    workingchannel = await get_working_channel(ctx)
    async with pool.acquire() as conn:
        if ctx.message.channel.id == workingchannel.id:
            if arg.upper() == "ON":
                onlyFree = True
                await conn.execute('UPDATE botdata SET onlyfree=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), onlyFree)
                await ctx.send("🆓 | I will look only for free deals.")
            elif arg.upper() == "OFF":
                onlyFree = False
                await conn.execute('UPDATE botdata SET onlyfree=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), onlyFree)
                await ctx.send("💰 | I will look for all deals.")
            else:
                await ctx.send("❌ | I didn't understand. Try again.")

@bot.command()
@commands.has_permissions(manage_guild=True)
async def currency(ctx, arg):
    """Adds information about other currencies."""

    workingchannel = await get_working_channel(ctx)
    async with pool.acquire() as conn:
        if ctx.message.channel.id == workingchannel.id:
            if arg.upper() == "OFF":
                actualCurrency = None
                await conn.execute('UPDATE botdata SET currency=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), actualCurrency)
                await ctx.send("❌ | Currency calculating switched off.")

            elif arg.upper() in currencyswitch.avaliblecurrencies:
                actualCurrency = arg.upper()
                await conn.execute('UPDATE botdata SET currency=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), actualCurrency)
                await ctx.send("💱 | Currency switched to " + arg.upper())
            else:
                await ctx.send("❌ | Selected currency doesnt exist or is not supported. Try again.")

@currency.error
async def currency_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("You have to type currency.")

@bot.command()
@commands.has_permissions(manage_guild=True)
async def consoles(ctx, args):
    """Specifies if bot should send informations about console deals. False by default."""

    workingchannel = await get_working_channel(ctx)
    async with pool.acquire() as conn:
        if ctx.message.channel.id == workingchannel.id:
            if args.upper() == 'ON':
                await conn.execute('UPDATE botdata SET consoles=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), True)
                await ctx.send(":white_check_mark: | Console deals are ON!")
            elif args.upper() == 'OFF':
                await conn.execute('UPDATE botdata SET consoles=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), False)
                await ctx.send(":white_check_mark: | Console deals are OFF!")
            else:
                await ctx.send("❌ | I didnt understood. Try again.")

@consoles.error
async def consoles_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("You forgot to write ON/OFF. Try again.")

@bot.command()
@commands.has_permissions(manage_guild=True)
async def percentoff(ctx, args):
    """Filters deals by percentage specified by user. Default 0."""

    workingchannel = await get_working_channel(ctx)
    async with pool.acquire() as conn:
        if ctx.message.channel.id == workingchannel.id:
            if args.upper() =='OFF':
                await conn.execute('UPDATE botdata SET percentoff=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), 0)
                await ctx.send(f":white_check_mark: | All avalible deals will be delivered.")
            else:
                percent = int(args)
                if percent > 100:
                    percent = 100
                await conn.execute('UPDATE botdata SET percentoff=$2 WHERE server_id=$1',
                                   str(ctx.message.guild.id), percent)
                await ctx.send(f":white_check_mark: | Only deals from {percent}% off will be delivered.")

@bot.command()
@commands.has_permissions(manage_guild=True)
async def currentconfig(ctx):
    """Shows information about actuall config"""

    workingchannel = await get_working_channel(ctx)
    if ctx.message.channel.id == workingchannel.id:
        async with pool.acquire() as conn:
            data = await conn.fetchrow(
                'SELECT inloop, currency, workingchannel, onlyfree,consoles, percentoff FROM botdata WHERE server_id=$1',
                str(ctx.message.guild.id))
        inloop = data['inloop']
        onlyFree = data['onlyfree']
        workingchannel = data['workingchannel']
        consoles = data['consoles']
        actualCurrency = data['currency']
        percentage = data['percentoff']

        #changing it a bit for a better formatting
        if inloop is True:
            inloop='YES'
        else:
            inloop='NO'
        if workingchannel is None:
            workingchannel = 'No channel specified.'
        if onlyFree is True:
            onlyFree = 'YES'
        else:
            onlyFree = 'NO'
        if consoles is True:
            consoles = 'NO'
        else:
            consoles = 'YES'

        await ctx.send(f'🔨 My working channel is **{bot.get_channel(int(workingchannel))}**.\n'
                       f':mag_right: Am I searching for deals now? - **{inloop}**\n'
                       f':chart_with_upwards_trend: I will look for deals with at least **{percentage}%** off.\n'
                       f'🆓 Should I look for free deals only? - **{onlyFree}**.\n'
                       f'🕹️ Should I look for PC deals only? - **{consoles}**.\n'
                       f'💱 Is there any special currency that I should convert money to? - **{actualCurrency}**.\n')

"""EVENTS"""

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Game(f"{prefix}scrhelp | {prefix}scrinvite"))
    print('Logged on as {0}!'.format(bot.user))
    print('Connected to {} servers and {} users'.format(len(bot.guilds), len(set(bot.get_all_members()))))

@bot.event
async def on_guild_join(guild):
    """Sending welcome message to first channel that bot can write to. Adding server to DBase"""

    for channel in guild.text_channels:
        if channel.permissions_for(guild.me).send_messages:
            await channel.send(f":wave: Hello! :wave:\n"
                               f"Im Scrooge and I heard that you want me to help you get more for less. \n"
                               f"First of all show me where I can work - use **{prefix}getdealshere** in that channel. Keep in mind that I can work only in **one** channel **per server**. \n"
                               f"If you want more information about configuration and avalible commands use **{prefix}scrhelp** \n")
            break
    logging.info(f"New guild {guild.id} added.")


@bot.event
async def on_guild_remove(guild):
    """Removing server from DBase"""

    global serversid
    async with pool.acquire() as conn:
        await conn.execute('DELETE FROM botdata WHERE server_id=$1', str(guild.id))
    serversid.remove(str(guild.id))
    logging.info(f"Guild {guild.id} was deleted.")

@bot.event
async def on_command_error(ctx, error):
    """Error handling"""

    if hasattr(ctx.command, 'on_error'):
        return

    ignored = (commands.CommandNotFound, commands.UserInputError)

    error = getattr(error, 'original', error)

    if isinstance(error, ignored):
        return

    elif isinstance(error, commands.MissingPermissions):
        return await ctx.send('You dont have permissions to use that command.')

    elif isinstance(error, commands.BotMissingPermissions):
        return await ctx.send('I dont correct permissions to do that.')

async def search_for_deals():
    """Thats where magic happens"""

    global pool

    await bot.wait_until_ready()

    #fix for connecting to HerokuDBase
    sslctx = ssl.create_default_context()
    sslctx.check_hostname = False
    sslctx.verify_mode = ssl.CERT_NONE
    pool = await asyncpg.create_pool(dburl, ssl=sslctx)

    #saving servers
    await save_already_connected()

    while not bot.is_closed():
        serverlist = serversid #copy because serversid size might change during iteration
        rawdeals = await dealsdata.get_latest_deals()
        for guild in serverlist:
            async with pool.acquire() as conn:
                data = await conn.fetchrow(
                    'SELECT inloop, currency, workingchannel, onlyfree,consoles, percentoff FROM botdata WHERE server_id=$1',
                    str(guild))
            if data is not None: #can be None in 1st iteration (had to initialise set) when bot gets kicked while in this for loop
                inloop = data['inloop']
                actualCurrency = data['currency']
                workingchannel = data['workingchannel']
                onlyFree = data['onlyfree']
                consoles = data['consoles']
                percentage = data['percentoff']
            else:
                continue

            #filtering consoledeals and percentages
            if consoles is True:
                deals = rawdeals
                deals = dealsdata.filter_deals_by_percentage(deals, int(percentage))
            else:
                deals = dealsdata.delete_console_deals(rawdeals)
                deals = dealsdata.filter_deals_by_percentage(deals, int(percentage))

            if inloop is True: #if user wants bot to do the work
                workingchannel = bot.get_channel(int(workingchannel))
                for key, value in deals.items():
                    if dealsdata.is_there_free_game(key):
                        embed = discord.Embed(
                            title="FREE GAME FOUND!",
                            description=key + "\n" + value,
                            colour=discord.Colour.red()
                        )
                        await workingchannel.send('@here 🆓 FREE GAME FOUND 🆓', embed=embed)
                    elif onlyFree is False:
                        if actualCurrency is None:
                            embed = discord.Embed(
                                title=" New discount found!",
                                description=key + "\n" + value,
                                colour=discord.Colour.red()
                            )
                            await workingchannel.send(embed=embed)
                        else:
                            embed = discord.Embed(
                                title="New discount found!",
                                description=currencyswitch.title_with_new_currency(actualCurrency,
                                                                                   key) + "\n" + value,
                                colour=discord.Colour.red()
                            )
                            await workingchannel.send(embed=embed)
        await asyncio.sleep(30*60) #30 minutes cycle

"""Utils"""

async def get_working_channel(ctx):
    async with pool.acquire() as conn:
        workingchannel = bot.get_channel(int(await conn.fetchval('SELECT workingchannel FROM botdata WHERE server_id=$1',
                                             str(ctx.message.guild.id))))
        return workingchannel

async def initialise_server_in_dbase(serverid):
    """Adds new servers to DBase. If somehow id is already in dbase, logs warning and nothing is happening"""

    async with pool.acquire() as conn:
        try:
            await conn.execute("INSERT INTO botdata (server_id) VALUES ($1)", serverid)
        except asyncpg.exceptions.UniqueViolationError as e:
            logging.warning(serverid + " that key is already in database")

async def save_already_connected():
    """Saves list of currently connected servers to variable"""
    global serversid
    for guild in bot.guilds:
        if str(guild.id) not in serversid:
            await initialise_server_in_dbase(str(guild.id))
        serversid.add(str(guild.id))

async def list_servers():
    await bot.wait_until_ready()
    while not bot.is_closed:
        print("Current stats:")
        print(f'Connected to {len(bot.guilds)} servers and {len(set(bot.get_all_members()))} users')
        await asyncio.sleep(600)

bot.loop.create_task((search_for_deals()))
bot.loop.create_task((list_servers()))
bot.run(token)
