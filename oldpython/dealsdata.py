from config import client_id, client_secret, password, username, user_agent
import praw
import time
import re
import asyncio

async def get_latest_deals():

    reddit = praw.Reddit(client_id=client_id,
                         client_secret=client_secret,
                         password=password,
                         user_agent=user_agent,
                         username=username)

    timenow = time.time()
    sometimeago = timenow - (30*60)
    deals=dict()

    subreddit = reddit.subreddit('GameDeals')
    newest25posts = subreddit.new(limit=25)
    for submission in newest25posts:
        if(submission.created_utc >= sometimeago):
            deals[submission.title]=submission.url
    return deals

def delete_console_deals(olddeals):
    deals=dict()
    for key, value in olddeals.items():
        if (is_there_console_deal(key)) is False:
            deals[key]=value
    return deals

def filter_deals_by_percentage(olddeals, percentage):
    deals=dict()
    for key, value in olddeals.items():
        if find_percentages(key) >= percentage:
            deals[key] = value
    return deals

#TODO: jak zrobić, żeby nie ucinało jak jest PC/xbox
#'[Newegg] Forza Horizon 4: Ultimate Edition Xbox / Win 10 [Digital Code] $69.99 with code EMCEEEX29': 'http://www.newegg.com/Product/Product.aspx?Item=N82E16832011389'
def is_there_console_deal(title):
    foundobj = re.search(r'(ps ?vita)|(PSStore)|(playstation)|(Nintendo)|(3DS)|(Switch)|(Wii)|(XO.*)|(ps\d?)|(xbox\d*)', title, re.IGNORECASE)
    if foundobj is None:
        return False
    else:
        return True

def is_there_free_game(title):
    foundobj = re.search(r'(100%)|(\(free\))', title, re.IGNORECASE)
    if foundobj is None:
        return False
    else:
        return True

def find_percentages(title):
    foundobj = re.findall(r'\d\d?%', title)
    if not foundobj:
        return 100
    else:
        foundobj = [re.sub("%","",percentage) for percentage in foundobj]
        foundobj = [int(number) for number in foundobj]
        return min(foundobj)