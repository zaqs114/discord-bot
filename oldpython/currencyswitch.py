import re
from decimal import Decimal
from forex_python.converter import CurrencyRates

avaliblecurrencies = ['USD', 'JPY', 'BGN', 'CZK', 'DKK', 'GBP', 'HUF', 'PLN', 'RON', 'SEK', 'CHF', 'ISK', 'NOK', 'HRK',
                      'RUB', 'TRY', 'AUD', 'BRL', 'CAD', 'CNY', 'HKD', 'IDR', 'ILS', 'INR', 'KRW', 'MXN', 'MYR', 'NZD',
                      'PHP', 'SGD', 'THB', 'ZAR']

def currency_swap(startingcurrency, amount, finalcurrency):
    c = CurrencyRates()

    foundobj = re.search(r'[0-9]+[.,]?[0-9]+|\d|[0-9]+[.,]?[0-9]+|\d', amount)
    comatodot = foundobj.group().replace(',', '.')
    convertedvalue = c.convert(startingcurrency, finalcurrency, float(comatodot))
    return round(Decimal(convertedvalue), 2)


def title_with_new_currency(finalcurrency, title):
    objectsfound = re.finditer(r'[\$£€]([0-9]+[.,]?[0-9]+|\d)|([0-9]+[.,]?[0-9]+|\d)[\$£€]', title)

    for itemsfound in objectsfound:
        value = itemsfound.group()
        if('$' in value):
            title = title.replace(value, value+" ("+str(
                currency_swap('USD', value, finalcurrency))+" "+finalcurrency+") ")
        elif('€' in value):
            title = title.replace(value, value + " (" + str(
                currency_swap('EUR', value, finalcurrency)) + " " + finalcurrency + ") ")
        elif('£' in value):
            title = title.replace(value, value + " (" + str(
                currency_swap('GBP', value, finalcurrency)) + " " + finalcurrency + ") ")

    return title
