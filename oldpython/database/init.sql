create table botdata
(
	server_id varchar(300) not null,
	workingchannel varchar(300),
	consoles boolean default false,
	currency varchar(300),
	inloop boolean default false,
	onlyfree boolean default false,
	percentoff integer default 0
);

